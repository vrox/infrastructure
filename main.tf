terraform {
  backend "http" {}

  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.12.0"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.2.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "docker" {
  host = "ssh://gitlab-deploy@${var.vrox_main_server_ip}:${var.vrox_main_server_ssh_port}"

  cert_path = ""
}

provider "gitlab" {
  token = var.gitlab_api_token
  base_url = "https://gitlab.com/api/v4/"
}

module "backup" {
  source = "./backup"
  gitlab_api_token = var.gitlab_api_token
  gitlab_backup_group_id = var.gitlab_backup_group_id
  gitlab_bot_email = var.gitlab_bot_email
  gitlab_group_id = var.gitlab_group_id
  passbolt_container = module.passbolt.container
  passbolt_db_container = module.passbolt.db_container
  passbolt_db_username = module.passbolt.db_username
  passbolt_db_password = module.passbolt.db_password
  passbolt_db_name = module.passbolt.db_name
  zammad_backup_volume_mountpoint = module.zammad.backup_volume_mountpoint
}

module "domain" {
  source = "./domain"
  cloudflare_api_token = var.cloudflare_api_token
  cloudflare_account_id = var.cloudflare_account_id
  vrox_main_server_ip = var.vrox_main_server_ip
}

module "passbolt" {
  source = "./passbolt"
  gitlab_api_token = var.gitlab_api_token
  vrox_main_server_ip = var.vrox_main_server_ip
  vrox_main_server_ssh_port = var.vrox_main_server_ssh_port
  docker_proxy_network_id = module.proxy.docker_proxy_network_id
}

module "proxy" {
  source = "./proxy"
  vrox_main_server_ip = var.vrox_main_server_ip
  vrox_main_server_ssh_port = var.vrox_main_server_ssh_port
  cloudflare_account_id = var.cloudflare_account_id
  cloudflare_api_token = var.cloudflare_api_token
}

module "zammad" {
  source = "./zammad"
  docker_proxy_network_id = module.proxy.docker_proxy_network_id
  gitlab_api_token = var.gitlab_api_token
  vrox_main_server_ip = var.vrox_main_server_ip
  vrox_main_server_ssh_port = var.vrox_main_server_ssh_port
}
