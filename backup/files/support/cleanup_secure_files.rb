#!/usr/bin/env ruby
# frozen_string_literal: true

require 'gitlab'

project_id = ARGV.shift
gitlab_endpoint = ENV.fetch('CI_API_V4_URL', nil)
gitlab_token = ENV.fetch('V_GITLAB_TOKEN', nil)

gitlab_client = Gitlab.client(endpoint: gitlab_endpoint, private_token: gitlab_token)

secure_files = gitlab_client.get("/projects/#{project_id}/secure_files").auto_paginate

now = Time.now

def difference_in_days(time1, time2)
  (time1 - time2) / 3600 / 24
end

files_to_delete = secure_files.select { |file| difference_in_days(now, Time.parse(file.created_at)) > 80 }

files_to_delete.each do |file|
  gitlab_client.delete("/projects/#{project_id}/secure_files/#{file.id}")
  puts "Deleted secure file with id #{file.id} and name #{file.name}"
end
