resource "gitlab_project" "backup_runner" {
  name             = "Backup Runner"
  path             = "backup-runner"
  description      = "This project is managed with Terrafrom by [Infrastructure](${data.gitlab_project.infrastructure.web_url})"
  namespace_id     = data.gitlab_group.vrox.id
  visibility_level = "public"

  analytics_access_level               = "disabled"
  container_registry_access_level      = "disabled"
  environments_access_level            = "disabled"
  feature_flags_access_level           = "disabled"
  forking_access_level                 = "disabled"
  infrastructure_access_level          = "disabled"
  issues_access_level                  = "disabled"
  merge_requests_access_level          = "disabled"
  monitor_access_level                 = "disabled"
  packages_enabled                     = false
  pages_access_level                   = "disabled"
  releases_access_level                = "disabled"
  requirements_access_level            = "disabled"
  security_and_compliance_access_level = "disabled"
  snippets_access_level                = "disabled"
  wiki_access_level                    = "disabled"

  public_builds = false
}

resource "gitlab_repository_file" "backup" {
  project             = gitlab_project.backup_runner.id
  file_path           = each.value
  branch              = gitlab_project.backup_runner.default_branch
  content             = filebase64("${path.module}/files/${each.value}")
  author_email        = var.gitlab_bot_email
  author_name         = "Taucher2003-Bot"
  commit_message      = "Update ${each.value}"
  overwrite_on_create = true

  for_each = fileset("${path.module}/files", "**/*")
}

resource "gitlab_repository_file" "base" {
  project   = gitlab_project.backup_runner.id
  file_path = ".gitlab-ci.yml"
  branch    = gitlab_project.backup_runner.default_branch
  content   = base64encode(
    <<-EOC
    include:
      - local: 'system/*.gitlab-ci.yml'

    stages:
      - backup
      - cleanup

    ${file("${path.module}/../.gitlab/ci/ssh.gitlab-ci.yml")}
    EOC
  )
  author_email        = var.gitlab_bot_email
  author_name         = "Taucher2003-Bot"
  commit_message      = "Update base pipeline"
  overwrite_on_create = true
}

data "gitlab_project_variable" "infrastructure" {
  project  = data.gitlab_project.infrastructure.id
  key      = each.value
  for_each = toset([
    "V_DEPLOY_SSH_KEY",
    "V_SSH_KNOWN_HOSTS",
    "TF_VAR_gitlab_api_token",
    "TF_VAR_vrox_main_server_ip",
  ])
}

//noinspection HILUnresolvedReference
resource "gitlab_project_variable" "backup_information" {
  key     = each.key
  project = gitlab_project.backup_runner.id
  value   = each.value

  for_each = {
    V_DEPLOY_SSH_KEY : sensitive(data.gitlab_project_variable.infrastructure["V_DEPLOY_SSH_KEY"].value)
    V_SSH_KNOWN_HOSTS : sensitive(data.gitlab_project_variable.infrastructure["V_SSH_KNOWN_HOSTS"].value)
    V_GITLAB_TOKEN : sensitive(data.gitlab_project_variable.infrastructure["TF_VAR_gitlab_api_token"].value)
    V_MAIN_SERVER_IP : sensitive(data.gitlab_project_variable.infrastructure["TF_VAR_vrox_main_server_ip"].value)
    V_PASSBOLT_CONTAINER : var.passbolt_container
    V_PASSBOLT_DB_CONTAINER : var.passbolt_db_container
    V_PASSBOLT_DB : var.passbolt_db_name
    V_PASSBOLT_BACKUP_PROJECT : gitlab_project.passbolt.id
    V_ZAMMAD_BACKUP_MOUNTPOINT : var.zammad_backup_volume_mountpoint
    V_ZAMMAD_BACKUP_PROJECT_PATH : gitlab_project.zammad.path_with_namespace
    V_ZAMMAD_DEPLOY_TOKEN_USER : gitlab_deploy_token.zammad.username
    V_ZAMMAD_DEPLOY_TOKEN_TOKEN : gitlab_deploy_token.zammad.token
  }
}

resource "gitlab_project_variable" "passbolt_mysql_config" {
  key     = "V_PASSBOLT_MYSQL_CONFIG"
  project = gitlab_project.backup_runner.id
  value   = <<-EOF
  [mysqldump]
  user = ${var.passbolt_db_username}
  password = "${var.passbolt_db_password}"
  EOF

  variable_type = "file"
}

resource "gitlab_pipeline_schedule" "backup_create" {
  project     = gitlab_project.backup_runner.id
  description = "Create backups"
  ref         = gitlab_project.backup_runner.default_branch
  cron        = "0 2 * * *"
  active      = false
}
