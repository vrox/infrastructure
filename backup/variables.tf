variable "gitlab_api_token" {
  type      = string
  sensitive = true
}

variable "gitlab_group_id" {
  type = number
}

variable "gitlab_backup_group_id" {
  type = number
}

variable "gitlab_bot_email" {
  type = string
}

variable "passbolt_db_name" {
  type = string
}

variable "passbolt_db_username" {
  type = string
}

variable "passbolt_db_password" {
  type      = string
  sensitive = true
}

variable "passbolt_db_container" {
  type = string
}

variable "passbolt_container" {
  type = string
}

variable "zammad_backup_volume_mountpoint" {
  type = string
}
