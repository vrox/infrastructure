terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.2.0"
    }
  }
}

data "gitlab_group" "vrox" {
  group_id = var.gitlab_group_id
}

data "gitlab_group" "vrox_private_backups" {
  group_id = var.gitlab_backup_group_id
}

//noinspection MissingProperty
data "gitlab_project" "infrastructure" {
  path_with_namespace = "${data.gitlab_group.vrox.full_path}/infrastructure"
}

resource "gitlab_project" "passbolt" {
  name             = "Passbolt"
  path             = "passbolt"
  namespace_id     = data.gitlab_group.vrox_private_backups.id
  visibility_level = "private"

  initialize_with_readme = false

  analytics_access_level               = "disabled"
  container_registry_access_level      = "disabled"
  environments_access_level            = "disabled"
  feature_flags_access_level           = "disabled"
  forking_access_level                 = "disabled"
  infrastructure_access_level          = "disabled"
  issues_access_level                  = "disabled"
  lfs_enabled                          = false
  merge_requests_access_level          = "disabled"
  monitor_access_level                 = "disabled"
  packages_enabled                     = false
  pages_access_level                   = "disabled"
  releases_access_level                = "disabled"
  requirements_access_level            = "disabled"
  security_and_compliance_access_level = "disabled"
  snippets_access_level                = "disabled"
  wiki_access_level                    = "disabled"
}

resource "gitlab_project" "zammad" {
  name             = "Zammad"
  path             = "zammad"
  namespace_id     = data.gitlab_group.vrox_private_backups.id
  visibility_level = "private"

  initialize_with_readme = false

  analytics_access_level               = "disabled"
  builds_access_level                  = "disabled"
  container_registry_access_level      = "private"
  environments_access_level            = "disabled"
  feature_flags_access_level           = "disabled"
  forking_access_level                 = "disabled"
  infrastructure_access_level          = "disabled"
  issues_access_level                  = "disabled"
  lfs_enabled                          = false
  merge_requests_access_level          = "disabled"
  monitor_access_level                 = "disabled"
  packages_enabled                     = false
  pages_access_level                   = "disabled"
  releases_access_level                = "disabled"
  repository_access_level              = "disabled"
  requirements_access_level            = "disabled"
  security_and_compliance_access_level = "disabled"
  snippets_access_level                = "disabled"
  wiki_access_level                    = "disabled"

  //noinspection HCLUnknownBlockType
  container_expiration_policy {
    enabled = true
    cadence = "1d"
    keep_n  = 10
    name_regex_delete = ".*"
    older_than = "7d"
  }
}

resource "gitlab_deploy_token" "zammad" {
  project = gitlab_project.zammad.id
  name = "Backup uploader"
  scopes = ["read_registry", "write_registry"]
}
