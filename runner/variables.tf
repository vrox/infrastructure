variable "vrox_main_server_ip" {
  type      = string
  sensitive = true
}

variable "vrox_main_server_ssh_port" {
  type = string
}

variable "gitlab_api_token" {
  type      = string
  sensitive = true
}

variable "gitlab_group_id" {
  type = number
}
