terraform {
  backend "http" {}

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.2.0"
    }
  }
}

provider "docker" {
  host = "ssh://gitlab-deploy@${var.vrox_main_server_ip}:${var.vrox_main_server_ssh_port}"

  cert_path = ""
}

provider "gitlab" {
  token = var.gitlab_api_token
  base_url = "https://gitlab.com/api/v4/"
}

data "gitlab_group" "vrox" {
  group_id = var.gitlab_group_id
}

resource "gitlab_runner" "default_runner" {
  registration_token = data.gitlab_group.vrox.runners_token
}

resource "docker_image" "runner" {
  name = "gitlab/gitlab-runner:v16.0.2"
}

resource "docker_container" "default_runner" {
  //noinspection HILUnresolvedReference
  image = docker_image.runner.image_id
  name  = "gitlab-runner"
  restart = "always"

  volumes {
    container_path = "/var/run/docker.sock"
    host_path      = "/var/run/docker.sock"
    read_only      = true
  }

  upload {
    file = "/etc/gitlab-runner/config.toml"
    //noinspection HILUnresolvedReference
    content = <<CONTENT
    concurrent = 1

    [[runners]]
      name = "Default Runner"
      url = "https://gitlab.com/"
      token = "${gitlab_runner.default_runner.authentication_token}"
      executor = "docker"
      [runners.docker]
        image = "alpine:latest"

    CONTENT
  }
}
