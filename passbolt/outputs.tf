output "db_username" {
  value = local.db_username
}

output "db_name" {
  value = local.db_name
}

output "db_password" {
  value = random_password.db.result
}

output "db_container" {
  value = docker_container.db.name
}

output "container" {
  value = docker_container.passbolt.name
}
