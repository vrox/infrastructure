terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.2.0"
    }
  }
}

locals {
  db_username = "passbolt"
  db_name = "passbolt"
}

data "gitlab_project_variable" "mail_password" {
  project = "vrox/secret-manager"
  key = "MAIL_PASSWORD_noreply_vrox_eu"
}

resource "random_password" "db" {
  length = 32
}

resource "docker_network" "passbolt" {
  name = "passbolt"
}

resource "docker_image" "db" {
  name = "mariadb:10.10"
}

resource "docker_volume" "db" {
  name = "passbolt_db"

  lifecycle {
    prevent_destroy = true
  }
}

resource "docker_container" "db" {
  //noinspection HILUnresolvedReference
  image   = docker_image.db.image_id
  name    = "passbolt_db"
  restart = "always"

  volumes {
    volume_name    = docker_volume.db.name
    container_path = "/var/lib/mysql"
  }

  networks_advanced {
    name = docker_network.passbolt.id
  }

  env = [
    "MYSQL_RANDOM_ROOT_PASSWORD=true",
    "MYSQL_DATABASE=${local.db_name}",
    "MYSQL_USER=${local.db_username}",
    "MYSQL_PASSWORD=${random_password.db.result}"
  ]
}

resource "docker_volume" "gpg" {
  name = "passbolt_gpg"

  lifecycle {
    prevent_destroy = true
  }
}

resource "docker_volume" "jwt" {
  name = "passbolt_jwt"

  lifecycle {
    prevent_destroy = true
  }
}

resource "docker_image" "passbolt" {
  name = "passbolt/passbolt:4.0.2-1-ce"
}

resource "docker_container" "passbolt" {
  //noinspection HILUnresolvedReference
  image   = docker_image.passbolt.image_id
  name    = "passbolt"
  restart = "always"

  depends_on = [docker_container.db]

  volumes {
    volume_name    = docker_volume.gpg.name
    container_path = "/etc/passbolt/gpg"
  }
  volumes {
    volume_name    = docker_volume.jwt.name
    container_path = "/etc/passbolt/jwt"
  }

  networks_advanced {
    name = var.docker_proxy_network_id
  }
  networks_advanced {
    name = docker_network.passbolt.id
  }

  //noinspection HILUnresolvedReference
  env = [
    "APP_FULL_BASE_URL=https://passbolt.vrox.eu",

    "DATASOURCES_DEFAULT_HOST=${docker_container.db.name}",
    "DATASOURCES_DEFAULT_USERNAME=${local.db_username}",
    "DATASOURCES_DEFAULT_PASSWORD=${random_password.db.result}",
    "DATASOURCES_DEFAULT_DATABASE=${local.db_name}",

    "EMAIL_DEFAULT_FROM_NAME=Vrox Passbolt",
    "EMAIL_DEFAULT_FROM=noreply@vrox.eu",
    "EMAIL_TRANSPORT_DEFAULT_HOST=mail.vrox.eu",
    "EMAIL_TRANSPORT_DEFAULT_USERNAME=noreply@vrox.eu",
    "EMAIL_TRANSPORT_DEFAULT_PASSWORD=${sensitive(data.gitlab_project_variable.mail_password.value)}",

    "VIRTUAL_HOST=passbolt.vrox.eu",
  ]

  command = ["/usr/bin/wait-for.sh", "-t", "0", "${docker_container.db.name}:3306", "--", "/docker-entrypoint.sh"]
}
