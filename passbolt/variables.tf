variable "vrox_main_server_ip" {
  type      = string
  sensitive = true
}

variable "vrox_main_server_ssh_port" {
  type = string
}

variable "gitlab_api_token" {
  type      = string
  sensitive = true
}

variable "docker_proxy_network_id" {
  type = string
}
