variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "cloudflare_account_id" {
  type      = string
  sensitive = true
}

variable "vrox_main_server_ip" {
  type      = string
  sensitive = true
}

variable "vrox_main_server_ssh_port" {
  type = string
}

variable "gitlab_api_token" {
  type      = string
  sensitive = true
}

variable "gitlab_group_id" {
  type = number
}

variable "gitlab_backup_group_id" {
  type = number
}

variable "gitlab_bot_email" {
  type = string
}
