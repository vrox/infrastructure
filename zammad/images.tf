resource "docker_image" "zammad" {
  name = "ghcr.io/zammad/zammad:6.0.0-66"
}

resource "docker_image" "postgresql" {
  name = "postgres:15.3-alpine"
}

resource "docker_image" "elasticsearch" {
  name = "bitnami/elasticsearch:8.8.0"
}

resource "docker_image" "memcached" {
  name = "memcached:1.6.20-alpine"
}

resource "docker_image" "redis" {
  name = "redis:7.0.5-alpine"
}
