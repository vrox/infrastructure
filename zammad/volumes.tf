resource "docker_volume" "elasicsearch" {
  name = "zammad_elasticseach"
}

resource "docker_volume" "postgresql" {
  name = "zammad_postgresql"
}

resource "docker_volume" "redis" {
  name = "zammad_redis"
}

resource "docker_volume" "zammad_backup" {
  name = "zammad_backup"
}

resource "docker_volume" "zammad_config_nginx" {
  name = "zammad_config_nginx"
}

resource "docker_volume" "zammad_var" {
  name = "zammad_var"
}

resource "docker_volume" "zammad_assets" {
  name = "zammad_assets"
}
