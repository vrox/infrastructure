//noinspection HILUnresolvedReference
resource "docker_container" "elasticsearch" {
  image = docker_image.elasticsearch.image_id
  name  = "zammad-elasticsearch"
  restart = "always"

  volumes {
    volume_name = docker_volume.elasicsearch.name
    container_path = "/bitnami/elasticsearch/data"
  }

  networks_advanced {
    name = docker_network.zammad.id
  }
}

//noinspection HILUnresolvedReference
resource "docker_container" "memcached" {
  image = docker_image.memcached.image_id
  name  = "zammad-memcached"
  restart = "always"

  networks_advanced {
    name = docker_network.zammad.id
  }
}

//noinspection HILUnresolvedReference
resource "docker_container" "postgresql" {
  image = docker_image.postgresql.image_id
  name  = "zammad-postgresql"
  restart = "always"

  volumes {
    volume_name = docker_volume.postgresql.name
    container_path = "/var/lib/postgresql/data"
  }

  env = [
    "POSTGRES_DB=zammad_production",
    "POSTGRES_USER=zammad",
    "POSTGRES_PASSWORD=zammad"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }
}

//noinspection HILUnresolvedReference
resource "docker_container" "redis" {
  image = docker_image.redis.image_id
  name  = "zammad-redis"
  restart = "always"

  volumes {
    volume_name = docker_volume.redis.name
    container_path = "/data"
  }

  networks_advanced {
    name = docker_network.zammad.id
  }
}

//noinspection HILUnresolvedReference
resource "docker_container" "zammad_init" {
  image = docker_image.zammad.image_id
  name  = "zammad-init"
  restart = "on-failure"
  command = ["zammad-init"]
  user = "0:0"

  must_run = false

  volumes {
    volume_name = docker_volume.zammad_config_nginx.name
    container_path = "/etc/nginx/sites-enabled"
  }
  volumes {
    volume_name = docker_volume.zammad_var.name
    container_path = "/opt/zammad/var"
  }

  env = [
    "MEMCACHE_SERVERS=${docker_container.memcached.name}:11211",
    "REDIS_URL=redis://${docker_container.redis.name}:6379",
    "POSTGRESQL_HOST=${docker_container.postgresql.name}",
    "POSTGRES_DB=zammad_production",
    "POSTGRES_USER=zammad",
    "POSTGRES_PASS=zammad"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }

  depends_on = [
    docker_container.postgresql
  ]
}

//noinspection HILUnresolvedReference
resource "docker_container" "zammad_rails" {
  image = docker_image.zammad.image_id
  name  = "zammad-rails"
  restart = "always"
  command = ["zammad-railsserver"]

  volumes {
    volume_name = docker_volume.zammad_var.name
    container_path = "/opt/zammad/var"
  }
  volumes {
    volume_name = docker_volume.zammad_assets.name
    container_path = "/opt/zammad/public/assets/images"
  }

  env = [
    "MEMCACHE_SERVERS=${docker_container.memcached.name}:11211",
    "REDIS_URL=redis://${docker_container.redis.name}:6379",
    "POSTGRESQL_HOST=${docker_container.postgresql.name}",
    "POSTGRES_DB=zammad_production",
    "POSTGRES_USER=zammad",
    "POSTGRES_PASS=zammad"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }

  depends_on = [
    docker_container.postgresql,
    docker_container.memcached,
    docker_container.redis
  ]
}

//noinspection HILUnresolvedReference
resource "docker_container" "zammad_scheduler" {
  image = docker_image.zammad.image_id
  name  = "zammad-scheduler"
  restart = "always"
  command = ["zammad-scheduler"]

  volumes {
    volume_name = docker_volume.zammad_var.name
    container_path = "/opt/zammad/var"
  }

  env = [
    "MEMCACHE_SERVERS=${docker_container.memcached.name}:11211",
    "REDIS_URL=redis://${docker_container.redis.name}:6379",
    "ZAMMAD_RAILSSERVER_HOST=${docker_container.zammad_rails.name}",
    "POSTGRESQL_HOST=${docker_container.postgresql.name}",
    "POSTGRES_DB=zammad_production",
    "POSTGRES_USER=zammad",
    "POSTGRES_PASS=zammad"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }

  depends_on = [
    docker_container.zammad_rails,
    docker_container.memcached,
    docker_container.redis
  ]
}

//noinspection HILUnresolvedReference
resource "docker_container" "zammad_websocket" {
  image = docker_image.zammad.image_id
  name  = "zammad-websocket"
  restart = "always"
  command = ["zammad-websocket"]

  volumes {
    volume_name = docker_volume.zammad_var.name
    container_path = "/opt/zammad/var"
  }

  env = [
    "MEMCACHE_SERVERS=${docker_container.memcached.name}:11211",
    "REDIS_URL=redis://${docker_container.redis.name}:6379",
    "ZAMMAD_RAILSSERVER_HOST=${docker_container.zammad_rails.name}",
    "POSTGRESQL_HOST=${docker_container.postgresql.name}",
    "POSTGRES_DB=zammad_production",
    "POSTGRES_USER=zammad",
    "POSTGRES_PASS=zammad"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }

  depends_on = [
    docker_container.zammad_rails,
    docker_container.memcached,
    docker_container.redis
  ]
}

//noinspection HILUnresolvedReference
resource "docker_container" "zammad_nginx" {
  image = docker_image.zammad.image_id
  name  = "zammad-nginx"
  restart = "always"
  command = ["zammad-nginx"]

  volumes {
    volume_name = docker_volume.zammad_config_nginx.name
    container_path = "/etc/nginx/sites-enabled"
    read_only = true
  }
  volumes {
    volume_name = docker_volume.zammad_var.name
    container_path = "/opt/zammad/var"
    read_only = true
  }
  volumes {
    volume_name = docker_volume.zammad_assets.name
    container_path = "/opt/zammad/public/assets/images"
  }

  env = [
    "VIRTUAL_HOST=ticket.vrox.eu",
    "NGINX_SERVER_SCHEME=https",
    "NGINX_PORT=80",
    "REDIS_URL=redis://${docker_container.redis.name}:6379",
    "ZAMMAD_RAILSSERVER_HOST=${docker_container.zammad_rails.name}",
    "ZAMMAD_WEBSOCKET_HOST=${docker_container.zammad_websocket.name}"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }
  networks_advanced {
    name = var.docker_proxy_network_id
  }

  depends_on = [
    docker_container.zammad_rails
  ]
}

//noinspection HILUnresolvedReference
resource "docker_container" "zammad_backup" {
  image = docker_image.zammad.image_id
  name  = "zammad-backup"
  restart = "always"
  command = ["zammad-backup"]
  entrypoint = ["/usr/local/bin/backup.sh"]

  volumes {
    volume_name = docker_volume.zammad_backup.name
    container_path = "/var/tmp/zammad"
  }
  volumes {
    volume_name = docker_volume.zammad_var.name
    container_path = "/opt/zammad/var"
    read_only = true
  }

  upload {
    source = "${path.module}/files/backup.sh"
    source_hash = filesha1("${path.module}/files/backup.sh")
    file = "/usr/local/bin/backup.sh"
    executable = true
  }

  upload {
    source = "${path.module}/files/setup_postgresql.sh"
    source_hash = filesha1("${path.module}/files/setup_postgresql.sh")
    file = "/usr/local/bin/setup_postgresql.sh"
    executable = true
  }

  env = [
    "BACKUP_TIME=03:00",
    "HOLD_DAYS=10",
    "POSTGRESQL_HOST=${docker_container.postgresql.name}",
    "POSTGRESQL_DB=zammad_production",
    "POSTGRESQL_USER=zammad",
    "POSTGRESQL_PASSWORD=zammad",
    "TZ=Europe/Berlin",
    "ZAMMAD_RAILSSERVER_HOST=${docker_container.zammad_rails.name}"
  ]

  networks_advanced {
    name = docker_network.zammad.id
  }

  depends_on = [
    docker_container.zammad_rails,
    docker_container.postgresql
  ]

  connection {
    host = var.vrox_main_server_ip
    port = var.vrox_main_server_ssh_port
    user = "gitlab-deploy"
    type = "ssh"
  }

  provisioner "remote-exec" {
    inline = [
      "docker exec --user root ${self.name} /usr/local/bin/setup_postgresql.sh"
    ]
  }
}
