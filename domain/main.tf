terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.12.0"
    }
  }
}

locals {
  simple_txt = {
    _dmarc                        = "v=DMARC1; p=none"
    _domainkey                    = "o=-"
    _github-challenge-vroxnetwork = "505955488c"
  }
  root_txt = [
    "v=spf1 ip4:185.223.31.153 include:spf.protection.outlook.com -all",
    "google-site-verification=Q_ssQoF2xfpqeiMisT7qr67kWZKaR5sbrkk8rn99kBE"
  ]
  plesk_cname = [
    "mail",
    "webmail",
    "support",
    "@",
    "www",
    "panel"
  ]
  main_server_cname = [
    "ticket",
    "passbolt",
  ]
}

data "cloudflare_zone" "main_domain" {
  account_id = var.cloudflare_account_id
  name       = "vrox.eu"
}

resource "cloudflare_record" "plesk" {
  name    = "plesk"
  type    = "A"
  zone_id = data.cloudflare_zone.main_domain.id
  value   = "185.223.31.153"
  proxied = false
}

resource "cloudflare_record" "main_server" {
  name    = "main_server"
  type    = "A"
  zone_id = data.cloudflare_zone.main_domain.id
  value   = var.vrox_main_server_ip
  proxied = true
}

resource "cloudflare_record" "plesk_cname" {
  name     = each.value
  type     = "CNAME"
  zone_id  = data.cloudflare_zone.main_domain.id
  value    = cloudflare_record.plesk.hostname
  for_each = toset(local.plesk_cname)
}

resource "cloudflare_record" "main_server_cname" {
  name     = each.value
  type     = "CNAME"
  zone_id  = data.cloudflare_zone.main_domain.id
  value    = cloudflare_record.main_server.hostname
  proxied  = true
  for_each = toset(local.main_server_cname)
}

resource "cloudflare_record" "_imap_tcp" {
  name    = "_imaps._tcp"
  type    = "SRV"
  zone_id = data.cloudflare_zone.main_domain.id
  data {
    name     = data.cloudflare_zone.main_domain.name
    service  = "_imaps"
    proto    = "_tcp"
    target   = "plesk12.zap-webspace.com"
    port     = 993
    weight   = 0
    priority = 0
  }
  proxied = false
}

resource "cloudflare_record" "_pop3s_tcp" {
  name    = "_pop3s._tcp"
  type    = "SRV"
  zone_id = data.cloudflare_zone.main_domain.id
  data {
    name     = data.cloudflare_zone.main_domain.name
    service  = "_pop3s"
    proto    = "_tcp"
    target   = "plesk12.zap-webspace.com"
    port     = 995
    weight   = 0
    priority = 0
  }
  proxied = false
}

resource "cloudflare_record" "_smtps_tcp" {
  name    = "_smtps._tcp"
  type    = "SRV"
  zone_id = data.cloudflare_zone.main_domain.id
  data {
    name     = data.cloudflare_zone.main_domain.name
    service  = "_smtps"
    proto    = "_tcp"
    target   = "plesk12.zap-webspace.com"
    port     = 465
    weight   = 0
    priority = 0
  }
  proxied = false
}

resource "cloudflare_record" "simple_txt" {
  name     = each.key
  type     = "TXT"
  zone_id  = data.cloudflare_zone.main_domain.id
  value    = each.value
  for_each = local.simple_txt
}

resource "cloudflare_record" "default_domainkey" {
  name    = "default._domainkey"
  type    = "TXT"
  zone_id = data.cloudflare_zone.main_domain.id
  value   = "v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC5tZnGNxinZAX2B2s2vMfip9aqostIozFvtaG0b0OVt/TSaL6jBOHkHBtDGeXDvc1K9yiKQmBcgVK93LS+VAM0H3BFHWpOIa1X+inlWitQQBIaE2UZ6YQsFgDUOoscXIEyG/5yCbHmhHo9LYOPByB6oMijhNHwofQlj3q9FBOwgwIDAQAB;"
}

resource "cloudflare_record" "root_txt" {
  name     = "@"
  type     = "TXT"
  zone_id  = data.cloudflare_zone.main_domain.id
  value    = each.value
  for_each = toset(local.root_txt)
}
