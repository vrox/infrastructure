variable "vrox_main_server_ip" {
  type      = string
  sensitive = true
}

variable "vrox_main_server_ssh_port" {
  type = string
}

variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "cloudflare_account_id" {
  type      = string
  sensitive = true
}
